{ pkgs ? import <nixpkgs> {}}: with pkgs;

dockerTools.buildImage {
  name = "markus1189/tools";
  tag = "latest";

  contents = [ curl coreutils jq bash ];

  config = {
    Cmd = [ "${pkgs.bash}/bin/bash" ];
    Env = [ "CURL_CA_BUNDLE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"];
  };
}
