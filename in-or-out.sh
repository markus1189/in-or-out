#!/usr/bin/env bash

function determinePayload() {
    local filename
    filename="$(date -d '+ 1 day' +%F).txt"

    if [[ -f "$filename" ]]; then
        echo "${filename}"
    else
        echo "default-body.txt"
    fi
}

function main() {
    if [[ -z "${TOKEN}" || -z "${CHANNEL}" ]] ; then
        echo "You have to set TOKEN and CHANNEL in your environment" >&2
        exit 1
    fi

    PAYLOAD="$(determinePayload)"

    echo "Posting to channel: '${CHANNEL}' with payload '${PAYLOAD}'"

    sed "s/:CHANNEL:/${CHANNEL}/g" "${PAYLOAD}" > payload.txt

    curl -si \
      --fail \
      -X POST \
      -H "Authorization: Bearer ${TOKEN}" \
      -H 'Content-type: application/json' \
      --data "@payload.txt" \
      https://slack.com/api/chat.postMessage
}

main
